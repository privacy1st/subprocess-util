#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import subprocess
import sys
import threading
from pathlib import Path
from queue import Queue
from typing import Callable, IO, AnyStr


def execute_produce_chunks(command: list[str],
                           get_chunk_path: Callable[[int], Path],
                           handle_chunk: Callable[[int, bool], None],
                           chunk_size: int = 1024 * 1024,
                           ) -> int:
    """
    Executes the given `command` in a subprocess.

    The stdout of the subprocess is saved in chunks.
    The location of the chunks is determined by `get_chunk_path(chunk_no)`.

    A separate thread calls `handle_chunk(chunk_no, last_chunk)` once a new chunk was saved.
    It is the duty of `handle_chunk` to delete the processed chunk.

    The stderr of the subprocess is printed to sys.stderr.

    :param command:
    :param get_chunk_path:
    :param handle_chunk:
    :param chunk_size:
    :return:
    """
    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        close_fds=True,
    )

    q = Queue(maxsize=2)

    threads = [
        threading.Thread(
            target=_stdout_worker,
            args=(process.stdout,
                  get_chunk_path,
                  q.put,
                  chunk_size,
                  )),
        threading.Thread(
            target=_chunk_worker,
            args=(q.get,
                  handle_chunk,
                  )),
        threading.Thread(
            target=_stderr_worker,
            args=(process.stderr,
                  )),
    ]

    for t in threads:
        t.daemon = True
        t.start()

    returncode: int = process.wait()
    for t in threads:
        t.join()

    return returncode


def _stdout_worker(binary_stdout: IO[AnyStr],
                   get_chunk_path: Callable[[int], Path],
                   queue_put: Queue.put,
                   chunk_size: int,
                   ):
    chunk_no: int = 1
    remaining_bytes: int = chunk_size
    chunk: bytes = b''

    while True:
        b = binary_stdout.read(remaining_bytes)
        if len(b) == 0:  # EOF reached.
            chunk_path: Path = get_chunk_path(chunk_no)
            _save_chunk(chunk, chunk_path)
            queue_put((chunk_no, True))

            break

        chunk += b
        chunk_len: int = len(chunk)
        if chunk_len == chunk_size:  # Next chunk is full.
            chunk_path: Path = get_chunk_path(chunk_no)
            _save_chunk(chunk, chunk_path)
            queue_put((chunk_no, False))

            chunk = b''
            remaining_bytes = chunk_size
            chunk_no += 1
        elif chunk_len < chunk_size:
            remaining_bytes = chunk_size - chunk_len
        else:
            raise ValueError('Invalid state')

    # TODO: Has this any effect?
    # binary_stdout.close()


def _save_chunk(chunk: bytes, chunk_path: Path):
    """
    Saves a chunk at the given path.
    """
    print(f'Saving chunk {chunk_path}')
    chunk_path.parent.mkdir(parents=True, exist_ok=True)

    # Fails if file does already exist.
    with open(chunk_path, 'xb') as f:
        f.write(chunk)


def _chunk_worker(queue_get: Queue.get,
                  handle_chunk: Callable[[int, bool], None],
                  ):
    """
    Calls handle_chunk(chunk_no, last_chunk) for each chunk
    that has been saved.
    """
    while True:
        chunk_no, last_chunk = queue_get()
        handle_chunk(chunk_no, last_chunk)
        if last_chunk:
            return


def _stderr_worker(binary_stderr: IO[AnyStr]):
    """
    Prints stderr of subprocess to sys.stderr.
    """
    b: bytes
    for b in binary_stderr:
        sys.stderr.write(f'[STDERR] {b.decode("UTF-8")}')

    # TODO: Has this any effect?
    # binary_stderr.close()
