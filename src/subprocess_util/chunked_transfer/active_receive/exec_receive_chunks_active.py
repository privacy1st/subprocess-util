import shlex
from pathlib import Path
from queue import Queue

from subprocess_util.chunked_transfer.active_receive.common import Message2, Message1
from subprocess_util.exec_consume_chunks import execute_consume_chunks
from subprocess_util.repeat import repeat_until_successful


def exec_receive_chunks_active(
        command: list[str],
        ssh_source: str,
        source_chunk_dir: Path,
        target_chunk_dir: Path,
) -> int:
    def get_source_chunk_path(chunk_no: int) -> Path:
        return source_chunk_dir.joinpath(f'{chunk_no}')

    def get_target_chunk_path(chunk_no: int) -> Path:
        return target_chunk_dir.joinpath(f'{chunk_no}')

    def get_source_created_path(chunk_no: int) -> Path:
        return source_chunk_dir.joinpath(f'{chunk_no}.COMPLETE')

    def get_source_transferred_path() -> Path:
        return source_chunk_dir.joinpath('SOCKET')

    def handle_chunks(queue_put: Queue.put):
        # TODO: get starting value of chunk_no as argument
        chunk_no = 1
        while True:
            target_chunk_path = get_target_chunk_path(chunk_no)
            source_chunk_path = get_source_chunk_path(chunk_no)
            chunk_transferred_sock_path = get_source_transferred_path()

            usr_confirmation_socket = target_chunk_path.parent.joinpath(f'{chunk_no}.SOCKET')

            chunk_created_path = get_source_created_path(chunk_no)
            chunk_created_cmd = ['ssh',
                                 ssh_source,
                                 f'cat {shlex.quote(str(chunk_created_path))} && rm {shlex.quote(str(chunk_created_path))}']
            messages = [Message1.OK.value, Message1.EOF.value]
            rsync_cmd = ['rsync',
                         f'{ssh_source}:{source_chunk_path}',
                         f'{target_chunk_path}']
            chunk_transferred_cmd = ['ssh',
                                     ssh_source,
                                     f"printf '{Message2.OK_STR.value}' | nc -U {shlex.quote(str(chunk_transferred_sock_path))}"]

            # Wait until next chunk can be transferred from sending side.
            #
            # It can happen, that the sending side has not yet saved the next chunk.
            # In that case `cat ...` will fail.
            # We will retry 3 times, each time after 5 seconds sleep.
            #
            # If all 4 `cat ...` attempts failed, we wait until the user has inspected the problem.
            msg = repeat_until_successful(chunk_created_cmd, usr_confirmation_socket, retries=3, retry_delay_seconds=5)
            if msg not in messages:
                raise ValueError(f'Invalid message: {msg}')
            last_chunk = msg == Message1.EOF.value

            # Transfer chunk.
            repeat_until_successful(rsync_cmd, usr_confirmation_socket)

            # Add chunk path to queue.
            queue_put((target_chunk_path, last_chunk))

            # Inform sending side about successful transfer.
            repeat_until_successful(chunk_transferred_cmd, usr_confirmation_socket)

            if last_chunk:
                break
            chunk_no += 1

    # Create the local chunk directory.
    if target_chunk_dir.exists():
        created_chunk_dir = False
    else:
        target_chunk_dir.mkdir(parents=True, exist_ok=False)
        created_chunk_dir = True

    returncode = execute_consume_chunks(
        command=command,
        handle_chunks=handle_chunks,
    )

    # If no errors occurred, delete the chunk directory.
    # But only if we created it ourselves!
    if returncode == 0 and created_chunk_dir:
        target_chunk_dir.rmdir()

    return returncode
