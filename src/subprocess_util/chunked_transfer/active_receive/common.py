#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from enum import Enum


class Message1(Enum):
    OK = 'OK\n'
    EOF = 'EOF\n'


class Message2(Enum):
    OK_BINARY = b'OK\n'
    OK_STR = 'OK\n'
