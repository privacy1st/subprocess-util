import shlex
from pathlib import Path

from subprocess_util.chunked_transfer.active_send.common import NextChunkEnum
from subprocess_util.exec_produce_chunks import execute_produce_chunks
from subprocess_util.repeat import repeat_until_successful


def exec_send_chunks_active(
        command: list[str],
        ssh_target: str,
        source_chunk_dir: Path,
        target_chunk_dir: Path,
        chunk_size: int = 128 * 1024 * 1024,
) -> int:
    def get_source_chunk_path(chunk_no: int) -> Path:
        return source_chunk_dir.joinpath(f'{chunk_no}')

    def get_target_chunk_path(chunk_no: int) -> Path:
        return target_chunk_dir.joinpath(f'{chunk_no}')

    def handle_chunk(chunk_no: int, last_chunk: bool):
        source_chunk_path = get_source_chunk_path(chunk_no)
        target_chunk_path = get_target_chunk_path(chunk_no)
        print(f'Handling chunk {source_chunk_path}')

        usr_confirmation_socket = source_chunk_path.parent.joinpath(f'{chunk_no}.SOCKET')
        target_socket = target_chunk_dir.joinpath('SOCKET')

        message: str = NextChunkEnum.EOF_STR.value if last_chunk else NextChunkEnum.OK_STR.value

        rsync_cmd = ['rsync',
                     str(source_chunk_path),
                     f'{ssh_target}:{str(target_chunk_path)}']
        inform_cmd = ['ssh',
                      ssh_target,
                      f"printf '{message}' | nc -U {shlex.quote(str(target_socket))}"]

        repeat_until_successful(rsync_cmd, usr_confirmation_socket)
        # Delete local chunk after it has been transferred.
        source_chunk_path.unlink(missing_ok=False)
        repeat_until_successful(inform_cmd, usr_confirmation_socket)

    # Create the local chunk directory.
    if source_chunk_dir.exists():
        created_chunk_dir = False
    else:
        source_chunk_dir.mkdir(parents=True, exist_ok=False)
        created_chunk_dir = True

    returncode = execute_produce_chunks(
        command=command,
        get_chunk_path=get_source_chunk_path,
        handle_chunk=handle_chunk,
        chunk_size=chunk_size,
    )

    # If no errors occurred, delete the chunk directory.
    # But only if we created it ourselves!
    if returncode == 0 and created_chunk_dir:
        source_chunk_dir.rmdir()

    return returncode
