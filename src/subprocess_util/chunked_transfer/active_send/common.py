#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from enum import Enum


class NextChunkEnum(Enum):
    OK_BINARY = b'OK\n'
    OK_STR = 'OK\n'
    EOF_BINARY = b'EOF\n'
    EOF_STR = 'EOF\n'
