import socket
from pathlib import Path
from queue import Queue

from subprocess_util.chunked_transfer.active_send.common import NextChunkEnum
from subprocess_util.exec_consume_chunks import execute_consume_chunks
from subprocess_util.unix_sock_input import accept_loop_until_message


def exec_receive_chunks_passive(
        command: list[str],
        target_chunk_dir: Path,
) -> int:
    def get_target_chunk_path(chunk_no: int) -> Path:
        return target_chunk_dir.joinpath(f'{chunk_no}')

    def handle_chunks(queue_put: Queue.put):
        target_socket = target_chunk_dir.joinpath('SOCKET')
        target_socket.parent.mkdir(parents=True, exist_ok=True)

        print(f'Listening on socket {target_socket}')
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.bind(str(target_socket))
        sock.listen(1)

        chunk_no = 1
        messages: list[bytes] = [NextChunkEnum.OK_BINARY.value, NextChunkEnum.EOF_BINARY.value]
        while True:
            msg = accept_loop_until_message(sock, messages)

            last_chunk = msg == NextChunkEnum.EOF_BINARY.value
            target_chunk_path = get_target_chunk_path(chunk_no)
            queue_put((target_chunk_path, last_chunk))

            if last_chunk:
                break
            chunk_no += 1

        print(f'Closing socket {target_socket}')
        sock.close()
        target_socket.unlink(missing_ok=False)

    # Create the local chunk directory.
    if target_chunk_dir.exists():
        created_chunk_dir = False
    else:
        target_chunk_dir.mkdir(parents=True, exist_ok=False)
        created_chunk_dir = True

    returncode = execute_consume_chunks(
        command=command,
        handle_chunks=handle_chunks,
    )

    # If no errors occurred, delete the chunk directory.
    # But only if we created it ourselves!
    if returncode == 0 and created_chunk_dir:
        target_chunk_dir.rmdir()

    return returncode
